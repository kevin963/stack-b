# Stack-B

To deploy the project in server:
1. Download code from gitlab
2. Enter source code folder first <br/>
`cd ~/stack-b/myapp`
3. Install required package by the following command <br/>
`npm install`
4. Run the following command to compile and launch web site <br/>
`npm run deploy`
5. Go to the following url to see the web site <br/>
http://ipaddress:3000/
or
Click the link on first assignment

import React from 'react'

/**
 * In the App class set state and handle event.
 * Then render compoments and pass state or event fucntion as 
 * component props.
 */
class App extends React.Component {

    constructor() {
        super()
        this.state = {
            countryList: [],
            cname: [],
            newCountryName: [],
            message: "",
            withMSG: false,
            isLoaded: false,

        }

        this.handleChange = this.handleChange.bind(this)
        this.handleAddclick = this.handleAddclick.bind(this)
        this.handleDelete = this.handleDelete.bind(this)

    }

    handleChange(event) {

        const { name, value, type } = event.target
        type === "text" ? this.setState({
            [name]: value
        }) :
            this.setState({
                cname: event.target[event.target.selectedIndex].value
            })
    }

    handleAddclick(event) {

    }

    handleDelete(event) {

        let cname = this.state.cname;
        event.preventDefault();
        const { name, value } = event.target;

        fetch('http://10.25.138.114/deleteCountry', {

            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: cname
            })
        })
            .then((responseJson) => {
                let filteredCountryList = this.state.countryNames.filter(item => item.name !== this.state.cname)

                this.setState({
                    countryList: filteredCountryList,
                    message: "Country '" + this.state.cname + "' is deleted!",
                    withMsg: true,
                }
                )
                console.log(cname)
                alert(this.state.cname + " deleted from the database");
                localStorage.setItem('data', JSON.stringify(filteredCountryList))
            })
            .catch((error) => {
                throw (error)
            })

    }

    componentDidMount() {
        //localStorage.removeItem('my_data')
        if (localStorage.getItem('my_data')) {
            let data = JSON.parse(localStorage.getItem('my_data'))
            this.setState({
                isLoaded: true,
                countryList: data
            })
        } else {
            let getCountryUrl = 'http://10.25.138.114/countries'
            console.log(getCountryUrl)
            fetch(getCountryUrl)
                .then(response => response.json())
                .then((data) => {
                    localStorage.setItem('my_data', JSON.stringify(data))
                    this.setState({
                        isLoaded: true,
                        countryList: data
                    })
                })
        }
    }

    render() {

        const options = this.state.countryList.map((country, index) =>
            <option
                key={index}
                value={country.name}
            >
                {country.name}
            </option>
        )
        return (
            <div>
                <form>
                    <select name="countrtDropdown"
                        onChange={this.handleChange}
                    >
                        <option value="">---Selete a country---</option>
                        {options}
                    </select>
                    <p>
                        Country Name: {this.state.cname}
                    </p>
                    {
                        this.state.withMsg &&
                        <AlertComponent message={this.state.message} />
                    }
                    <br></br>
                    <button type="button" class="btn btn-danger" onClick={this.handleDelete}>Delete Country</button>
                    <br />
                    <hr />
                    <br />
                    <p>Update Country Name</p>
                    <input type="text" class="form-control" id="usr"></input>
                    <button type="button" class="btn btn-warning" onClick={this.handleUpdate}>Update Country</button>
                    <br />
                    <hr />
                    <br />
                    <p>Add Country</p>
                    <input type="text" class="form-control" id="newcountry" value={this.state.value}></input>
                    <button type="button" class="btn btn-success" onClick={this.handleAdd}>Add Country</button>
                </form>
            </div>
        )
    }
}

export default App

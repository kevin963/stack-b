

/* ReactDOM.render(
  <div>Country Name</div>,
  document.getElementById('container')
); */

class Hello extends React.Component {
  render() {
    return <h1>{this.props.message}</h1>;
  }
}

ReactDOM.render (
  <Hello message = 'hello my friend' />,
  document.getElementById("root")
);